package com.applicationa.fauna.vacuum;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class VacuumController {
    
    @RequestMapping("/")
    public String index() {
        return "Greetings from Appa Vacuum!";
    }
    
}